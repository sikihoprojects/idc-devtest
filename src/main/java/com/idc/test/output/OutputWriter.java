package com.idc.test.output;

import java.io.IOException;
import java.io.Writer;
import java.util.List;
import java.util.Map;

public interface OutputWriter {

    void export(Map<String, List<OutputData>> output, Writer writer) throws IOException;
}
