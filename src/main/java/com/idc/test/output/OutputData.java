package com.idc.test.output;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@Builder
@EqualsAndHashCode
public class OutputData {
    private final String vendor;
    private final long units;
    private  double share;
}
