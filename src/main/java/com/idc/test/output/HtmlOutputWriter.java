package com.idc.test.output;

import java.io.IOException;
import java.io.Writer;
import java.util.List;
import java.util.Map;

//export format could be nicer of cource, maybe some template framework to use , eg. thymeleaf etc..
public class HtmlOutputWriter implements OutputWriter {
    @Override
    public void export(Map<String, List<OutputData>> output, Writer writer) throws IOException {
        writer.append("<html><head></head><body>");
        for (Map.Entry<String,List<OutputData>> entry: output.entrySet()) {
            writer.append("<h2>");
            writer.append(entry.getKey());
            writer.append("</h2>");
            writer.append("<table><tr><th>Vendor</th><th>Unit</th><th>Share</th>");
            for (OutputData o : entry.getValue()) {
                writer.append("<tr>");
                writer.append("<td>" + o.getVendor() + "</td>");
                writer.append("<td>" + o.getUnits() + "</td>");
                writer.append("<td>" + o.getShare() + "</td>");
                writer.append("</tr>");
            }
            writer.append("</table>");
        }
        writer.append("</body></html>");
    }
}
