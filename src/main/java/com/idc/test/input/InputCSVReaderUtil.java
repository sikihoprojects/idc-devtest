package com.idc.test.input;

import com.opencsv.bean.CsvToBean;
import com.opencsv.bean.CsvToBeanBuilder;

import java.io.Reader;
import java.util.List;
import java.util.stream.Collectors;

public class InputCSVReaderUtil {

    /**
     * parse csv file into Java objects. In case of uneffectivity rewrite to parse and process way
     */
    public static List<InputData> readAll(Reader reader) {
        CsvToBean<Object> lines = new CsvToBeanBuilder<>(reader)
                .withType(InputData.class)
                .withIgnoreLeadingWhiteSpace(true)
                .build();

        return lines.stream().map(l->(InputData) l).collect(Collectors.toList());
    }
}
