package com.idc.test.input;

import com.opencsv.bean.CsvBindByName;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class InputData {
    @CsvBindByName(column = "Country", required = true)
    private String country;
    @CsvBindByName(column = "Timescale", required = true)
    private String timescale;
    @CsvBindByName(column = "Vendor", required = true)
    private String vendor;
    @CsvBindByName(column = "Units", required = true)
    private double units;


}
