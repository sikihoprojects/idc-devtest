package com.idc.test;

import com.idc.test.action.ActionType;
import com.idc.test.input.InputCSVReaderUtil;
import com.idc.test.input.InputData;
import lombok.SneakyThrows;

import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

/**
 * Example of parameters e.g. data.csv get_by_ven "2010 Q3" Dell
 */
public class Main {


    @SneakyThrows
    public static void main(String[] args) {
        if (args.length < 2) {
            throw new IllegalArgumentException("1st arg is filename, 2nd arg is action [export|get_by_ven|get_rows_by_ven|sort_by_vendor|sort_by_units]");
        }

        String filename = args[0];
        System.out.println("Filename: " + filename);


        ActionType type = ActionType.fromString(args[1]);
        if (type == null ) {
            throw new IllegalArgumentException("Unexpected arg 2 " + ActionType.values());
        }

        Reader reader = Files.newBufferedReader(Paths.get(filename));
        List<InputData> lines = InputCSVReaderUtil.readAll(reader);

        System.out.println("Result: "+type.getAction().process(lines, args));

        System.out.println("Done successful");

    }
}
