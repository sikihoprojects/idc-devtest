package com.idc.test.action;

import com.idc.test.input.InputData;
import com.opencsv.CSVWriter;
import lombok.SneakyThrows;

import java.io.FileWriter;
import java.io.Writer;
import java.util.LinkedList;
import java.util.List;

/**
 * Sorts and export lines from input files
 */
public abstract class AbstractSortAction implements Action {
    private static final String OUT = "out.csv";

    @Override
    @SneakyThrows
    public String process(List<InputData> data, String[] args) {
        if (args.length != 2) {
            return "Params length is not correct;";
        }

        sort(data);
        Writer writer = new FileWriter(OUT);

        try (CSVWriter csvWriter
                     = new CSVWriter(writer, ',', CSVWriter.NO_QUOTE_CHARACTER, CSVWriter.NO_ESCAPE_CHARACTER, "\n")) {

            csvWriter.writeAll(toStringArray(data));
        }
        return OUT;
    }

    abstract void sort(List<InputData> data);

    private static List<String[]> toStringArray(List<InputData> data) {
        List<String[]> outputData = new LinkedList<>();

        // adding header record
        outputData.add(new String[]{"Country", "Timescale", "Vendor", "Units"});


        data.forEach(d -> outputData.add(new String[]{d.getCountry(), d.getTimescale(), d.getVendor(), String.valueOf(d.getUnits())}));


        return outputData;
    }


}
