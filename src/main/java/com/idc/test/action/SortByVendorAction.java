package com.idc.test.action;

import com.idc.test.input.InputData;

import java.util.Comparator;
import java.util.List;

public class SortByVendorAction  extends AbstractSortAction implements Action {

    @Override
    void sort(List<InputData> data) {
        data.sort(Comparator.comparing(InputData::getVendor));
    }


}
