package com.idc.test.action;

import com.idc.test.input.InputData;
import com.idc.test.output.OutputData;
import com.idc.test.transform.Transaformer;
import lombok.SneakyThrows;

import java.text.MessageFormat;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public class GetShareAction implements Action {
    private static final String OUT = "{0} units and {1}%";


    @Override
    @SneakyThrows
    public String process(List<InputData> data, String[] args) {
        if (args.length != 4) {
            throw new IllegalArgumentException("Params length is not correct;");
        }

        //TODO maybe filter specific period to be more efficient, depends on the data size
        Map<String, List<OutputData>> periods = Transaformer.transaform(data);

        List<OutputData> vendors = periods.get(args[2]);
        if (vendors == null) {
            throw new IllegalArgumentException("No vendors found in this period.");
        }
        Optional<OutputData> res = vendors.stream().filter(v->v.getVendor().equals(args[3])).findFirst();
        if (!res.isPresent()) {
            throw new IllegalArgumentException("No vendor found in this period.");
        }
        return MessageFormat.format(OUT, res.get().getUnits(),res.get().getShare());
    }
}
