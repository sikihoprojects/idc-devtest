package com.idc.test.action;

import com.idc.test.input.InputData;

import java.util.List;

public class GetRowAction implements Action {
    @Override
    public String process(List<InputData> data, String[] params) {
        if (params.length != 3) {
            throw new IllegalArgumentException( "Params length is not correct;");
        }
        String vendorName = params[2];
        StringBuilder sb = new StringBuilder(vendorName);
        sb.append(" on rows: ");
        int counter = 0;
        for (InputData d : data) {
            counter++;
            if (d.getVendor().equals(vendorName)) {
                sb.append(counter);
                sb.append(", ");
            }
        }
        if (sb.length() == 0) {
            sb.append("No match found");
        }
        return sb.toString();
    }
}
