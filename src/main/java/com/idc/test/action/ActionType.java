package com.idc.test.action;

import lombok.Getter;

import java.util.Arrays;

@Getter
public enum ActionType {
    EXPORT("export", new ExportAction()),
    GET_BY_VENDOR("get_by_ven", new GetShareAction()),
    GET_ROWS_BY_VENDOR("get_rows_by_ven", new GetRowAction()),
    SORT_BY_VENDOR("sort_by_vendor", new SortByVendorAction()),
    SORT_BY_UNIT("sort_by_units", new SortByUnitAction());
    private final String name;
    private final Action action;

    ActionType(String name, Action action) {
        this.action = action;
        this.name = name;
    }

    public static ActionType fromString(String arg) {
        return Arrays.stream(ActionType.values()).filter(v->v.name.equals(arg)).findFirst().orElse(null);
    }
}
