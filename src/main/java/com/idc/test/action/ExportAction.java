package com.idc.test.action;

import com.idc.test.input.InputData;
import com.idc.test.output.CsvOutputWriter;
import com.idc.test.output.ExcelOutputWriter;
import com.idc.test.output.HtmlOutputWriter;
import com.idc.test.output.OutputData;
import com.idc.test.output.OutputWriter;
import com.idc.test.transform.Transaformer;
import lombok.SneakyThrows;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.Writer;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ExportAction implements Action {
    private static final String OUT = "out.html";
    private static final Map<String, OutputWriter> FORMATS = new HashMap<>();
    static {
        FORMATS.put("html", new HtmlOutputWriter());
        FORMATS.put("csv", new CsvOutputWriter());
        FORMATS.put("excel", new ExcelOutputWriter());
    }


    @Override
    @SneakyThrows
    public String process(List<InputData> data, String[] args) {
        if (args.length != 3) {
            throw new IllegalArgumentException("Params length is not correct;");
        }


        OutputWriter writer = FORMATS.get(args[2]);
        if (writer== null) {
            throw new IllegalArgumentException("Args[2] must containts one of [html|csv|excel]");
        }

        Map<String, List<OutputData>> outputLines = Transaformer.transaform(data);

        try (
            Writer wr  = new BufferedWriter(new FileWriter(OUT, false))) {
            writer.export(outputLines, wr);
        }

        return OUT;
    }
}
