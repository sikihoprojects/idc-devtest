package com.idc.test.action;

import com.idc.test.input.InputData;

import java.util.List;

/**
 * Represents user action to process.
 */
public interface Action {

    /**
     *
     * @param data lines from input file in Java objects
     * @param args arguments from cmd
     * @return
     */
    String process(List<InputData> data, String[] args);
}
