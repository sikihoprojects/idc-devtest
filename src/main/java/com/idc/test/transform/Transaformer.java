package com.idc.test.transform;

import com.idc.test.input.InputData;
import com.idc.test.output.OutputData;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Transforms data to the specific format, if I use spring, this would not be static.
 */
public class Transaformer {
    public static Map<String, List<OutputData>> transaform(List<InputData> lines) {
        Map<String, List<OutputData>> periods = new HashMap<>();

        for (InputData d : lines) {
            List<OutputData> vendors = periods.get(d.getTimescale());
            if (vendors == null) {
                vendors = new LinkedList<>();
                periods.put(d.getTimescale(), vendors);
            }
            vendors.add(OutputData.builder().units((long) d.getUnits()).vendor(d.getVendor()).build());
        }

        calculateShares(periods);



        return periods;
    }

    private static void calculateShares(Map<String, List<OutputData>> periods) {
        for (List<OutputData> vendors : periods.values()) {
            double total = 0;
            for (OutputData d : vendors) {
                total += d.getUnits();
            }

            for (OutputData d : vendors) {
                d.setShare(BigDecimal.valueOf(d.getUnits()/total*100).setScale(2, RoundingMode.HALF_UP).doubleValue());//could be nicer
            }
        }
    }
}
